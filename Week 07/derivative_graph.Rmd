---
title: "График функции и касательных к ней в R"
author: "Шалаев Н. Е."
date: '15 июня 2019 г.'
output:
  pdf_document:
    latex_engine: xelatex
  html_document: default
header-includes:
  - \usepackage{fontspec}
  - \setmainfont{PT Mono}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Задача

В этом примеры мы посмотрим, как с помощью R рисовать графики функций и касательные к ним. Для этого нам потребуется рассмотреть символьное дифференцирование в R и обращение с полученными выражениями.

# Простой пример

Возьмем простейшую функцию:

$$y = x^2$$

Объявим её в виде функции в R (для того, чтобы сделать дальнейшие шаги компактней):

```{r}
f <- function(x) {
    return(x^2)
}
```

Построим график функции:

```{r out.width = '66%', fig.align='center'}
x_range <- seq (-5,5,0.1)
plot(x_range, 
     f(x_range), 
     lwd = 2, type = "l")
```

Возьмём производную. Для этого воспользуемся функцией `D()`, которая сразу возвращает результат в виде выражения, которое можно будет вычислить с подстановкой значений (вывод `deriv()`, как вы видели ранее, гораздо более громоздкий):

```{r}
derivative <- D(expression(x^2), "x")
derivative
```

Теперь построим касательные к графику функции в нескольких точках. Для этого нам нужно будет провести одно дополнительное вычисление — поскольку мы знаем только _наклон_ касательной в той или иной точке на графике функции, нам необходимо вычислить также и смещение прямой, чтобы она через эту точку прошла.

Решая уравнение касательной относительно b

$$ y = ax + b $$
получаем:
$$ b = y - ax $$

Отсюда построение графика получается прямолинейным:

```{r out.width = '66%', fig.align='center'}
plot(x_range, 
     f(x_range), 
     lwd = 2, type = "l")
x <- 3
abline(a = f(x) - eval(derivative)*x, b = eval(derivative), col = "blue")
x <- -2
abline(a = f(x) - eval(derivative)*x, b = eval(derivative), col = "yellow")
```

Как можно заметить, здесь конкретные величины x задавались глобально с помощью имени переменной. В качестве альтернативы можно указывать эти значения в виде второго аргумента функции `eval()` — например, `eval(derivative, list("x" = 3))`

# Более сложный пример

$$y = \frac{x^2}{x^e}$$

Число Эйлера в R как отдельная константа не выделено, но его всегда можно получить как `exp(1)`.
```{r}
f2 <- function(x) {
  return ( (x^2) / (x^exp(1)) )
}
```

Получим производную:
```{r}
derivative_2 <- D(expression(x^2/x^e), "x")
derivative_2
```

Построим график и касательные:

```{r out.width = '66%', fig.align='center'}
x_range <- seq(0,10,0.1)
plot(x_range, 
     f2(x_range), 
     lwd = 2, type = "l")
e <- exp(1)
x <- 3
abline(a = f2(x) - eval(derivative_2)*x, b = eval(derivative_2), col = "blue")
x <- 1
abline(a = f2(x) - eval(derivative_2)*x, b = eval(derivative_2), col = "green")
x <- 0.25
abline(a = f2(x) - eval(derivative_2)*x, b = eval(derivative_2), col = "yellow")
```

А можно ли построить график самой производной? Да, для этого вычисление выражения нужно провести на значениях нужного интервала (а не отдельных значениях):

```{r out.width = '66%', fig.align='center'}
x_range <- seq(0,10,0.1)
plot(x_range, 
     eval(derivative_2, list("x" = x_range, "e" = exp(1))), 
     lwd = 2, type = "l", col = "blue")
```

# Работа с deriv()

Всё вышеперечисленное можно сделать и при использовании функции `deriv()`, однако добраться до нужного выражения в глубине её результата не очень просто:

```{r}
derivative <- deriv(~ x*2, "x")
derivative
```

С глобальной подстановкой значения x:
```{r}
x <- 3
attr(eval(derivative), "gradient")
```

Или с подстановкой по месту:
```{r}
attr(eval(derivative, list("x" = 3)), "gradient")
```