---
title: "Векторные вычисления в R"
author: "Шалаев Н. Е."
date: '28 февраля 2019 г '
output:
  pdf_document:
    latex_engine: xelatex
  html_document: default
header-includes:
  - \usepackage{fontspec}
  - \setmainfont{PT Mono}
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Создание векторов

В R вектор является базовым типом данных. Следует отметить, что в смысле программирования большинство операций в R являются _векторными_, т.е. применяющими некоторое действие к большому числу входных данных, не требуя использовать для их перебора циклы. Для создания векторов используется функция `c()`:

```{r}
vector1 <- c(1,2,3,4,5,6,7,8,9)
vector2 <- c(11,22,33,44,55,66,77,88,99)
```

Оператор `<-` выполняет присваивание (т.е. запоминание данных под указанным именем). В дальнейшем примеры исходят из того, что `vector1` и `vector2` имеют вышеуказанные значения.
```{r}
vector1
vector2
```
Также векторы могут оказываться результатом работы многих функций, например функции `seq()`:
```{r}
seq(1,20,3)
```

Наверное, не будет преувеличением сказать, что последовательная обработка векторов разными функциями является одной из основ работы в R.

## Простые операции

### Сложение и вычитание векторов

```{r}
vector1 + vector2
vector1 - vector2
```

### Умножение и деление вектора на скаляр (число)

```{r}
vector1 * 5
vector2 / 11
```

### Умножение и деление вектора на вектор (вспомогательная операция в R)

В данном случае R совершает действие, не имеющее эквивалента в математике, которое, впрочем, может оказаться полезным в каких-то практических целях:
```{r}
vector1 * vector2
vector1 / vector2
```

## Векторные операции

### Внутреннее (скалярное) произведение векторов

$$ \textbf{v} \cdot \textbf{u} = \sum_{i=1}^{n}{v_iu_i} $$

Поскольку R воспринимает "простое" умножение вектора на вектор как попарное умножение элементов векторов, для получения "настоящего" скалярного произведения нужно добавить функцию суммирования:

```{r}
sum(vector1 * vector2)
```

Также можно пользоваться операцией %*%:

```{r}
vector1 %*% vector2
```

Впрочем, эта операция является лишь алиасом для функции `crossprod(v,u)`:

```{r}
crossprod(vector1, vector2)
```

Можно отметить, что это название не очень удачно, т.к. оно похоже на cross-product (векторное произведение), не имея при этом к нему отношения.

### Векторное произведение векторов

$$ u \times v = \left[ u_2v_3 - u_3v_2, u_3v_1 - u_1v_3, u_1v_2 - u_2v_1 \right] $$

В чистом R функция для вычисления векторного произведения не определена. К счастью, в самом простом виде это несложно сделать самому:

```{r}
vxp <- function(u,v){
	return(c(u[2] * v[3] - u[3] * v[2], 
		   u[3] * v[1] - u[1] * v[3], 
		   u[1] * v[2] - u[2] * v[1]))
}

vxp(c(1,2,3), c(4,5,6))
```

Однако такая функция есть в пакете `pracma`, и называется она `cross()`:

```{r}
library(pracma)

cross(c(1,2,3), c(4,5,6))
```

### Внешнее произведение векторов

Настоящее внешнее произведение (хотя внешним иногда называют и векторное) — операция, из векторов размером 1×k и 1×n получающая матрицу размером k×n, являющаяся эквивалентом матричного умножения.

$$ u \otimes v = \begin{bmatrix} u_1 \\ u_2 \\ \dots \\ u_k \end{bmatrix} \begin{bmatrix} v_1&v_2&\dots&v_n \end{bmatrix} = \begin{bmatrix} u_1v_1&u_1v_2&\dots&u_1v_n \\ u_2v_1&u_2v_2&\dots&u_2v_n \\ \dots&\dots&\dots&\dots\\ u_kv_1&u_kv_2&\dots&u_kv_n \end{bmatrix} $$

```{r}
vector1 %o% vector2
```

## Норма

### Векторная норма

$$ \|v\| = (\sum_{i=1}^{n}v_i^2)^{1/2}$$

В чистом R такой функции нет, однако её несложно вычислить, используя функции суммирования `sum()` и извлечения квадратного корня `sqrt()`:

```{r}
sqrt(sum(vector1^2))
sqrt(sum(vector2^2))
```

### P-норма

В общем виде т.н. p-норма содержит корень произвольной степени p:

$$ \|v\| = (\sum_{i=1}^{n}v_i^2)^{1/p}$$

В библиотеке `pracma` есть функция `Norm(v,p)` для обобщенной нормы. По умолчанию, p = 2, и т.о. функция Norm вычисляет обычную норму с квадратным корнем.

```{r}
library(pracma)

Norm(vector1, 2)
Norm(vector2, 2)
```

Однако то же самое можно сделать и стандартными средствами, в буквальном смысле возведя сумму в дробную степень (отметим, что вычисление дроби должно быть заключено в скобки, т.к. иначе операция возведения в степень будет иметь более высокий приоритет):

```{r}
sum(vector1^2)^(1/2)
sum(vector2^2)^(1/2)
```

### Норма для p = ∞

В данном случае величиной нормы является самый большой элемент вектора:

$$ \|v\|_\infty = \max\limits_{{1}\leq{i}\leq{n}}(v_i) $$

В R для этого существует функция `max()`:

```{r}
max(vector1)
max(vector2)
```

Или же с помощью функции `Norm()`:

```{r}
library(pracma)

Norm(vector1, Inf)
Norm(vector2, Inf)
```