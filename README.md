# Math in Political Science

## Важные новости!

### Дедлайн для работ к экзамену

С учётом сложившейся обстановки дедлайн перенесён на 23 июня, 23 ч 59 мин.  
Т.о. для завершения/ревизии/переработки работ остаётся 4 полных дня (20, 21, 22 и 23).  
Список рекомендаций для доработки исследований [тут: FIX.md](FIX.md)

### Темы для теста ФЭПО

[См. отдельный файл FEPO.md](FEPO.md)

## Материалы к лекциям

Материалы к лекциям разбиты по неделям. В основном они состоят из примеров на R, которые были разобраны на занятиях. Есть несколько полезных статей.

* [Неделя 1](Week 01)
  + Булева алгебра и QCA
  + Формулы комбинаторики
  + Индексы коалиционного потенциала
* [Неделя 2](Week 02)
  + Тригонометрические функции и графики в полярных координатах
* [Неделя 3](Week 03)
  + Операции с векторами
  + Операции с матрицами
* [Неделя 4](Week 04)
  + Матрицы: решение систем уравнений, линейная регрессия, цепи Маркова
* [Неделя 5](Week 05)
  + Пределы, производные, интегрирование
* [Неделя 6](Week 06)
  + ничего
* [Неделя 7](Week 07)
  + Производные, графики функций и касательные к ним
* [Неделя 8](Week 08)
  + Сравнение адекватности регрессионных моделей
* [Неделя 9](Week 09)
  + Эффективное число партий
  + Отклонение от пропорциональности
* [Неделя 10](Week 10)
  + R² и нелинейная зависимость
  + Построение графиков функций в R
* [Неделя 11](Week 11)
  + Модель электоральной волатильности
* [Неделя 12](Week 12)
  + Отображения (немного о хаосе)
  + Метод Монте-Карло

## Почта

Напоминаю, что e-mail относительно курса следует снабжать темой, которая начинается с "(ВМ)" (этим же может и заканчиваться). В особенности это касается писем с работами.

Правильно:
* (ВМ)
* (ВМ) Вопрос по теме
* (ВМ) Имярек - не знаю, чего делать????777семь

Неправильно:
* (ДД)
* 
* {FM} ???
* ,,,=(^.^)=,,,
* Как нам обустроить Россию? (◡‿◡✿)
* Спасите-Помогите (Имярек И.И. 7-ой курс группа 3/5)


## Литература

Наглядная статистика. Используем R!  
А. Б. Шипунов, Е. М. Балдин, П. А. Волкова, А. И. Коробейников, С. А. Назарова, С. В. Петров, В. Г. Суфиянов  
2014 г.  
_Распространяется свободно, копия лежит рядом (Shipunov-rbook.pdf)._

Ахтямов А.М.  
Математика для социологов и экономистов  
2004 г.  

Кабаков Р.И.  
R в действии  
2014 г.  

Последние две книги можно найти в архиве books.zip

## Программы, которые использовались/упоминались

### R

https://cran.rstudio.com/

### RStudio

https://www.rstudio.com/products/rstudio/download/#download

### Gephi

https://gephi.org/users/download/

### Golly (game of life)

https://sourceforge.net/projects/golly/files/golly/

### Libre Office

https://ru.libreoffice.org/download/

### Graphviz

https://www.graphviz.org/download/

### Gnuplot

http://www.gnuplot.info/

### 7-zip

https://sourceforge.net/projects/sevenzip/files/7-Zip/

## Полезные сайты

https://www.wolframalpha.com/

https://dreampuf.github.io/GraphvizOnline

http://www.webgraphviz.com/

## Работа к экзамену

Подробности относительно работы к экзамену см. в файле EXAM.md

## Консультация

Консультация перенесена на 19 июня.

Обязательно посетите! Даже если кому-то покажется к тому моменту, что всё пропало.

